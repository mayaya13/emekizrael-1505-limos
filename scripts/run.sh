#!/bin/sh

set -e
. ./scripts/iso.sh
 
objcopy --strip-debug sysroot/boot/limos.kernel limos.sym
qemu-system-$(./scripts/target-triplet-to-arch.sh $HOST) -cdrom limos.iso -debugcon file:info.log
