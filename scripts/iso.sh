#!/bin/sh
set -e
. ./scripts/build.sh
 
mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub
 
cp sysroot/boot/limos.kernel isodir/boot/limos.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "limos" {
	multiboot /boot/limos.kernel
}
EOF
grub-mkrescue -o limos.iso isodir