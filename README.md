# LimOS - A hobby research operating system

This operating system is our 12th grade final project in the Magshimim program. 
The aim of building this project was to learn and explore the world of low level & operating system development & methodologies.

The OS is Unix-based, written for the x86 architecture in C++ and Assembly.


## Summary

- [LimOS - A hobby research operating system](#limos---a-hobby-research-operating-system)
  - [Summary](#summary)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installing](#installing)
  - [Running the tests](#running-the-tests)
  - [Deployment](#deployment)
  - [Built With](#built-with)
  - [Contributing](#contributing)
  - [Versioning](#versioning)
  - [Authors](#authors)
  - [License](#license)
  - [Acknowledgments](#acknowledgments)

## Getting Started

These instructions will get you a copy of the project up and running on
your local machine for development and testing purposes. See deployment
for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

- QEMU, inorder to run / debug the operating system (we havent tested running on real hardware yet)
- xorriso is some weird package which is required for grub stuff, type sudo "apt install xorriso" and everything should be fine.


### Installing

Firstly, you should set yourself up with a build of gcc

    ./scripts/build_gcc.sh

Then, you should run the operating system

    ./scripts/run.sh

## Running the tests

We will perhaps run some tests for heap allocations, but currently there are no tests.

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

- We havent chosen any license yet

## Contributing

As this is a personal project, we do not aim to open source it. After we finish Magshimim that might happen.

## Versioning

The single version is in the `develop` branch.

## Authors

- **Lior Yehezkely** - *Built the foundations of the system, most of the stuff related to interrupts & a heap allocator* -
    [lior5654|FiveSixFiveFour](https://github.com/lior5654)
- **Maya Katzir** - *Built the foundations of the system and everything related to paging and a PIT driver* -
    [maye](maya put some link here)

## License

we need to choose a license

## Acknowledgments

- Hat tip to our awsome mentor, F4dora0fD00m who guided us in the wonderful world of OS development & research with good sources and tips
- Lee-Or Saar was our head team manager & helped in managing our agile working method & deadlines, also gave useful tips.
