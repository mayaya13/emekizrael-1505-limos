template<class U, class V> class Pair
{
public:
    U first; V second;
    Pair(U first, V second) first(first), second(second);
};