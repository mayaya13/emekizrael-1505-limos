#pragma once

#include <libc/sys/cdefs.h>
#include <libc/common.h>

NO_MANGLE __attribute__((__noreturn__)) void abort(void);
