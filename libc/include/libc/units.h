#pragma once

#include <libc/common.h>
namespace Units
{
    inline namespace DataSizes
    {
        u64 operator ""_bytes(u64 bytes) {
            return bytes;
        }
        u64 operator ""_kilobytes(u64 kbytes) {
            return kbytes * 1024LL;
        }
        u64 operator ""_megabytes(u64 mbytes) {
            return mbytes * 1024LL * 1024LL;
        }

    }
}