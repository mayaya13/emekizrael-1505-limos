#pragma once

#include <libc/sys/cdefs.h>
#include <libc/common.h>
#include <libc/string.h>

#include <kernel/tty.h> // FIXME: for usermode we need syscalls instead, check user/kernel with __is_libk
//extern void TTY::putchar();
//extern void TTY::setcolor(u8, u8);
#define EOF (-1)

using __out_func_type = i32(*)(i32);
using __color_func_type = void(*)(u8, u8);

template<__out_func_type OutFunc>
inline static void __print_s(const char* s, u64 length)
{
    for(u64 i = 0; i < length; ++i)
    {
        OutFunc(s[i]);
    }
}
template<__out_func_type OutFunc, u8 base, typename T>
inline static void __print_unum(T val)
{
    static_assert(base<=16, "__print_s: Base not supported!");
    static_assert(base>=2, "__print_s: Base not supported!");
    const u8 MAX_BASE=16;
    const u8 BUF_SIZE=64;
    const char ALPHABET[MAX_BASE+1] = "0123456789ABCDEF";
    char conv_buffer[BUF_SIZE];
    u8 curr_pos = BUF_SIZE-1;
    do
    {
        conv_buffer[curr_pos--] = ALPHABET[val % base];
        val /= base;
    } while(val);
    curr_pos++;
    while(curr_pos < BUF_SIZE)
    {
        OutFunc(conv_buffer[curr_pos++]);
    }


}
template<__out_func_type OutFunc, __color_func_type ColorFunc> 
i32 printfmt(const char* __restrict format, va_list parameters)
{
    while(*format != NULL)
    {
        if(*format == '%')
        {
            switch(format[1])
            {
                case NULL:
                    OutFunc('%');
                    break;
                case 'c':
                    OutFunc((char)va_arg(parameters, i32));
                    format++;
                    break;
                case 's':
                {
                    const char* s = va_arg(parameters, const char*);
                    __print_s<OutFunc>(s, strlen(s));
                    format++;
                    break;
                }
                case 'd':
                {
                    i32 num = va_arg(parameters, i32);
                    if(num < 0)
                    {
                        OutFunc('-');
                        num *= -1;
                    }
                    __print_unum<OutFunc, 10, u32>(num);
                    format++;
                    break;
                }
                case 'u':
                    __print_unum<OutFunc, 10, u32>(va_arg(parameters, u32));
                    format++;
                    break;
                case 'x':
                    __print_unum<OutFunc, 16, u32>(va_arg(parameters, u32));
                    format++;
                    break;
                case 'o':
                    __print_unum<OutFunc, 8, u32>(va_arg(parameters, u32));
                    format++;
                    break;
                case 'b':
                    __print_unum<OutFunc, 2, u32>(va_arg(parameters, u32));
                    format++;
                    break;
                case 'l':
                {
                    i64 num = va_arg(parameters, i64);
                    if(num < 0)
                    {
                        OutFunc('-');
                        num *= -1LL;
                    }
                    __print_unum<OutFunc, 10, u64>(num);
                    format++;
                    break;
                }
                default:
                    OutFunc('%');
                    OutFunc(*format);
                    format++;
                
            }
        }
        #ifdef __is_libk
            else if (*format == '$')
            {
                switch(format[1])
                {
                    case NULL:
                        OutFunc('$');
                        break;
                    case 'C':
                    {
                        u8 fg = (u8)va_arg(parameters, i32);
                        u8 bg = (u8)va_arg(parameters, i32);
                        ColorFunc(fg, bg);
                        format++;
                        break;
                    }
                    default:
                        OutFunc('$');
                        OutFunc(format[1]);
                        format++;
                }
            } 
        #endif
        else
        {
            OutFunc(*format);
        }
        format++;
    }
    return 0; // FIXME: this should be the number of letters printed / error code, I am feeling lazy rn
}
NO_MANGLE i32 printf(const char* __restrict s, ...);
NO_MANGLE i32 putchar(i32);
NO_MANGLE i32 puts(const char*);
