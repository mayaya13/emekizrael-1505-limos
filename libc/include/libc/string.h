#pragma once

#include <libc/sys/cdefs.h>
#include <libc/common.h>

NO_MANGLE i32 memcmp(const void*, const void*, u64);
NO_MANGLE void* memcpy(void* __restrict, const void* __restrict, u64);
NO_MANGLE void* memmove(void*, const void*, u64);
NO_MANGLE void* memset(void*, int, u64);
NO_MANGLE u64 strlen(const char*);
