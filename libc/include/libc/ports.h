#pragma once

#include <libc/common.h>

// FIXME: this is architecture dependent?
namespace ports
{
    /*
    @ [?] port output
    @ [>] const u8& __value: the value to be outputted
    @ [>] const u16& __port: the port to output to
    @ [<] noreturn
    */
    inline void out(const u8& __value, const u16& __port)
    {
        asm volatile ("outb %b0,%w1": :"a" (__value), "Nd" (__port));
    }

    /*
    @ [?] port input
    @ [>] const u16& __port: the port to get input from
    @ [<] u8: the value from the port
    */
    inline u8 in(const u16& __port)
    {
        u8 _v;
        asm volatile ("inb %w1,%0":"=a" (_v):"Nd" (__port));
        return _v;
    }

    // io_wait concept from https://wiki.osdev.org/Inline_Assembly/Examples#I.2FO_access
    /*
    @ [?] wait for an io operation to complete
    @ [<] noreturn
    */
    inline void wait()
    {
        asm volatile ( "outb %%al, $0x80" : : "a"(0) );
    }

}
