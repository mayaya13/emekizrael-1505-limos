#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stddef.h>

#define NO_MANGLE extern "C"
#define NO_ALIGNMENT __attribute__((__packed__))
#define CDECL __attribute__((__cdecl__))
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define ASM_FUNC NO_MANGLE CDECL
#define VA_ARGS(...) , ##__VA_ARGS__

typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;
typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t i8;
typedef u32 size_t;
