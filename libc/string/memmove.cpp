#include <libc/string.h>
 
void* memmove(void* dstptr, const void* srcptr, u64 size)
{
    u8* dst = (u8*) dstptr;
    const u8* src = (const u8*) srcptr;
	if (dst < src) 
	{
        for (u64 i = 0; i < size; i++)
		{
			dst[i] = src[i];
		}
	} 
	else
	{
        for (u64 i = size; i != 0; i--)
		{
			dst[i-1] = src[i-1];
		}
	}
	return dstptr;
}
