#include <libc/string.h>

void* memset(void* bufptr, int value, u64 size)
{
    u8* buf = (u8*)bufptr;
    for (u64 i = 0; i < size; i++)
	{
        buf[i] = (u8)value;
	}
	return bufptr;
}
