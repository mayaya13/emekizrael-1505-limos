#include <libc/string.h>

i32 memcmp(const void* aptr, const void* bptr, u64 size)
{
    const u8* a = (const u8*) aptr;
    const u8* b = (const u8*) bptr;
    for (u64 i = 0; i < size; i++) {
		if (a[i] < b[i])
		{
			return -1;
		}
		else if (b[i] < a[i])
		{
			return 1;
		}
	}
	return 0;
}
