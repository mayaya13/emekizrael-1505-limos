#include <libc/string.h>

void* memcpy(void* __restrict dstptr, const void* __restrict srcptr, u64 size)
{
    u8* dst = (u8*) dstptr;
    const u8* src = (const u8*) srcptr;
    for (u64 i = 0; i < size; i++)
	{
		dst[i] = src[i];
	}
	return dstptr;
}
