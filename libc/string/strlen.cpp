#include <libc/string.h>

u64 strlen(const char* str)
{
    u64 len = 0;
	while (str[len])
	{
		len++;
	}
	return len;
}
