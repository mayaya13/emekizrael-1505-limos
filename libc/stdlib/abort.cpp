#include <libc/stdio.h>
#include <libc/stdlib.h>

__attribute__((__noreturn__))
void abort(void)
{
#if defined(__is_libk)
	// TODO: Add proper kernel panic.
	printf("kernel: panic: abort()\n");
#else
	// TODO: Abnormally terminate the process as if by SIGABRT.
	printf("abort()\n");
#endif
    while (true) { }
	__builtin_unreachable();
}
