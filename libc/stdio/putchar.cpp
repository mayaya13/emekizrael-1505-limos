#include <libc/stdio.h>

#if defined(__is_libk)
#include <kernel/tty.h>
#endif
 
i32 putchar(i32 ic)
{
#if defined(__is_libk)
    TTY::putchar(ic);
#else
	// TODO: Implement stdio and the write system call, not needed until usermode.
#endif
	return ic;
}
