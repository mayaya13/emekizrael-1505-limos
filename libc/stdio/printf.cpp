#include <libc/stdio.h>

NO_MANGLE i32 printf(const char* __restrict format, ...)
{
    // FIXME: usermode syscalls instead of TTY
    va_list parameters;
    va_start(parameters, format);
    i32 retval = printfmt<TTY::putchar, TTY::setcolor>(format, parameters);
    va_end(parameters);
    return retval;
}
