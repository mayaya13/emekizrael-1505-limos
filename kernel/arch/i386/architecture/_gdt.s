.intel_syntax noprefix

.include "arch/i386/common/_asm_defs.s"

.equ param_ptr, 8

.section .text
    .globl gdt_flush
    .type gdt_flush, @function

gdt_flush:
    push ebp
    mov ebp, esp
    cli
    mov eax, [ebp+param_ptr]

    lgdt [eax]
    jmp KERNEL_CODE_SELECTOR:gdt_complete_flush

gdt_complete_flush:
    mov ax, KERNEL_DATA_SELECTOR
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    pop ebp
    ret
