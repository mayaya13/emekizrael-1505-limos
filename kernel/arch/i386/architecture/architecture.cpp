#include <arch/i386/architecture/architecture.h>
#include <libc/units.h>
using namespace Units::DataSizes;

const char* Architecture::NAME = "Intel i386 Architecture";
const u32 Architecture::PAGE_FRAME_BYTES = 4_kilobytes;

const u32 Architecture::NULL_SELECTOR = GDT::NULL_SELECTOR;
const u32 Architecture::CODE_SELECTOR = GDT::CODE_SELECTOR;
const u32 Architecture::DATA_SELECTOR = GDT::DATA_SELECTOR;
const u32 Architecture::CODE_FLAGS = GDT::CODE_FLAGS;
const u32 Architecture::DATA_FLAGS = GDT::DATA_FLAGS;
const u32 Architecture::CODE_LIMIT = GDT::CODE_LIMIT;
const u32 Architecture::DATA_LIMIT = GDT::DATA_LIMIT;
const u32 Architecture::CODE_BASE = GDT::CODE_BASE;
const u32 Architecture::DATA_BASE = GDT::DATA_BASE;

/*
@ [?] Initialize the Global Descriptor Table
@ [<] noreturn
*/
void Architecture::init_gdt()
{
    GDT::setup();
}

/*
@ [?] Initialize general architecture specific things
@ [<] noreturn
*/
void Architecture::initialize()
{
    Architecture::init_gdt();
}
