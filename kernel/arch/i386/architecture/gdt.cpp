#include <arch/i386/architecture/gdt.h>

const u32 GDT::MAX_ADDRESSABLE_UNIT = 0xfffffu;
const GDT::gdt_entry GDT::NULL_DESCRIPTOR_ENTRY = GDT::gdt_entry{.raw=0};
const u8 GDT::USERMODE_DPL = 3;
const u8 GDT::KERNELMODE_DPL = 0;
const u32 GDT::BASE_ADDRESS = 0x0;

const u32 GDT::NULL_SELECTOR = 0x0;
const u32 GDT::CODE_SELECTOR = 0x8;
const u32 GDT::DATA_SELECTOR = 0x10;
const u32 GDT::CODE_FLAGS = GDT::GDT_Flags::PRESENT |
                            GDT::GDT_Flags::KERNELMODE |
                            GDT::GDT_Flags::CODE_SEGMENT |
                            GDT::GDT_Flags::EXECUTABLE |
                            GDT::GDT_Flags::READ |
                            GDT::GDT_Flags::FOUR_BYTE_SEGMENT |
                            GDT::GDT_Flags::PAGE_GRANULARITY;

const u32 GDT::DATA_FLAGS = GDT::GDT_Flags::PRESENT |
                       GDT::GDT_Flags::KERNELMODE |
                       GDT::GDT_Flags::DATA_SEGMENT |
                       GDT::GDT_Flags::WRITE |
                       GDT::GDT_Flags::FOUR_BYTE_SEGMENT |
                       GDT::GDT_Flags::PAGE_GRANULARITY;

const u32 GDT::CODE_LIMIT = GDT::MAX_ADDRESSABLE_UNIT;
const u32 GDT::DATA_LIMIT = GDT::MAX_ADDRESSABLE_UNIT;
const u32 GDT::CODE_BASE = GDT::BASE_ADDRESS;
const u32 GDT::DATA_BASE = GDT::BASE_ADDRESS;


GDT::gdt_entry GDT::gdt_entries[GDT::MAX_ENTRIES];

/*
@ [?] Insert an entry to the gdt
@ [>] const u32& index: the index for the entry to be inserted
@ [>] const u32& limit: the segment descriptor limit
@ [>] const u32& base: the segment descriptor base
@ [>] const u32& flags: flags for setting up the descriptor (see: gdt::gdt_flags)
@ [<] noreturn
*/
void GDT::insert(u32 index, u32 limit, u32 base, u32 flags)
{
    KERNEL_LOG("index: %d limit: %x base: %x flags: %b", index, limit, base, flags);
    GDT::gdt_entry entry;
    entry.limit_low = limit & 0xffff;
    entry.limit_high = (limit & 0xf0000) >> 16;
    entry.base_low = (base & 0xffff);
    entry.base_middle = (base & 0xff0000) >> 16;
    entry.base_high = (base & 0xff000000) >> 24;
    entry.is_accessed = 0;
    entry.rw_bit = (flags & (GDT::GDT_Flags::READ | GDT::GDT_Flags::WRITE)) ? 1 : 0;
    entry.dc_bit = (flags & (GDT::GDT_Flags::DIRECTION | GDT::GDT_Flags::CONFORMING)) ? 1 : 0;
    entry.executable_bit = (flags & (GDT::GDT_Flags::EXECUTABLE)) ? 1 : 0;
    entry.descriptor_type = (flags & (GDT::GDT_Flags::CODE_SEGMENT | GDT::GDT_Flags::DATA_SEGMENT)) ? 1 : 0;
    if(flags & GDT::GDT_Flags::USERMODE)
    {
        entry.descriptor_privilege_level = USERMODE_DPL;
    }
    else
    {
        entry.descriptor_privilege_level = KERNELMODE_DPL;
    }
    entry.is_present = (flags & (GDT::GDT_Flags::PRESENT)) ? 1 : 0;
    entry.extra_flag_bit = 0; 
    entry.long_bit = 0;
    entry.size_bit = (flags & (GDT::GDT_Flags::FOUR_BYTE_SEGMENT)) ? 1 : 0;
    entry.granularity = (flags & (GDT::GDT_Flags::PAGE_GRANULARITY)) ? 1 : 0;

    GDT::insert(index, entry);
}

/*
@ [?] Insert an entry to the gdt
@ [>] const u32& index: the index for the entry to be inserted
@ [>] const gdt_entry& entry: the gdt_entry to be inserted
@ [<] noreturn
*/
void GDT::insert(u32 index, const gdt_entry& entry)
{
    KERNEL_LOG("%d %b %b", index, (entry.raw >> 32LL), entry.raw & 0xFFFFFFFF );
    gdt_entries[index] = entry;
}

/*
@ [?] Update the cpu with the current gdt
@ [<] noreturn
*/
void GDT::flush()
{
    KERNEL_LOG("%l", GDT::gdt_entries[0].raw);
    KERNEL_LOG("%l", GDT::gdt_entries[1].raw);
    KERNEL_LOG("%l", GDT::gdt_entries[2].raw);
    gdt_ptr_inners info;
    info.base = (u32)GDT::gdt_entries;
    info.limit =  sizeof(GDT::gdt_entry) * MAX_ENTRIES - 1;
    gdt_flush(&info);
}

/*
@ [?] Initialize the Global Descriptor Table in the default setup (flat mode)
@ [<] noreturn
*/
void GDT::setup()
{
    KERNEL_LOG("Initializing the Global Offset Table");
    GDT::insert(GDT::NULL_SELECTOR/8, 0u, 0u, 0u);
    GDT::insert(GDT::CODE_SELECTOR/8, GDT::CODE_LIMIT, GDT::CODE_BASE, GDT::CODE_FLAGS);
    GDT::insert(GDT::DATA_SELECTOR/8, GDT::DATA_LIMIT, GDT::DATA_BASE, GDT::DATA_FLAGS);
    KERNEL_LOG("flushing gdt")
    GDT::flush();
    KERNEL_LOG("gdt flushed")
    KERNEL_LOG("Global Offset Table Initialized");
}