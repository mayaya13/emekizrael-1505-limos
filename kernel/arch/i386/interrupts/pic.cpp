#include <arch/i386/interrupts/pic.h>

const u16 PIC::CHIP_COMMAND_PORTS[2] = { 0x20, 0xA0 };
const u16 PIC::CHIP_DATA_PORTS[2] = { 0x21, 0xA1 };
const u8 PIC::END_OF_INTERRUPT_CMD = 0x20;
const u8 PIC::ICW1 = 0x10 | 0x1;
const u8 PIC::ICW4 = 0x1;
const bool PIC::MASTER = 0;
const bool PIC::SLAVE = 1;
const u8 PIC::READ_IRR_CMD = 0x0a;
const u8 PIC::READ_ISR_CMD = 0x0b;

/*
@ [?] get the irq ignorance mask of one of the pic chips
@ [>] bool is_slave: whether the chip is master or slave
@ [<] u8: the mask
*/
u8 PIC::get_mask(bool is_slave)
{
    KERNEL_LOG("slave: %b", is_slave);
    return ports::in(PIC::CHIP_DATA_PORTS[is_slave]);
}

/*
@ [?] set the irq ignorance mask of one of the pic chips
@ [>] bool is_slave: whether the chip is master or slave
@ [>] const u8& mask: the mask to be set
@ [<] noreturn
*/
void PIC::set_mask(bool is_slave, u8 mask)
{
    KERNEL_LOG("slave: %b, mask: %d", is_slave, mask);
    ports::out(mask, PIC::CHIP_DATA_PORTS[is_slave]);
}

/*
@ [?] signal end of interrupt to pics 
@ [>] const bool& is_slave_interrupt: whether the interrupt involves the slave (irq >= 8)
@ [<] noreturn
*/
void PIC::end_of_interrupt(bool is_slave_interrupt)
{
    if (is_slave_interrupt)
    {
        ports::out(PIC::END_OF_INTERRUPT_CMD , PIC::CHIP_COMMAND_PORTS[PIC::SLAVE]);
    }
    ports::out(PIC::END_OF_INTERRUPT_CMD , PIC::CHIP_COMMAND_PORTS[PIC::MASTER]);
}

/*
@ [?] ignore a certain irq
@ [>] u8 irq: the irq to be ignored
@ [<] noreturn
*/
void PIC::ignore_irq(u8 irq)
{
    KERNEL_LOG("IRQ: %d", irq);
    u8 mask = 0;
    bool chip = irq < 8 ? PIC::MASTER : PIC::SLAVE;
    irq &= 0x7;
    mask = get_mask(chip);
    mask |= (1 << irq);
    PIC::set_mask(chip, mask); 
}

/*
@ [?] get interrupt request register value
@ [<] u16: register value (concatinated for both pics)
*/
u16 PIC::ger_irr()
{
    ports::out(PIC::READ_IRR_CMD, PIC::CHIP_COMMAND_PORTS[PIC::MASTER]);
    ports::out(PIC::READ_IRR_CMD, PIC::CHIP_COMMAND_PORTS[PIC::SLAVE]);
    return (ports::in(PIC::CHIP_COMMAND_PORTS[PIC::SLAVE]) << 8) | 
            ports::in(PIC::CHIP_COMMAND_PORTS[PIC::MASTER]);
}

/*
@ [?] get in-service register value
@ [<] u16: register value (concatinated for both pics)
*/
u16 PIC::get_isr()
{
    ports::out(PIC::READ_ISR_CMD, PIC::CHIP_COMMAND_PORTS[PIC::MASTER]);
    ports::out(PIC::READ_ISR_CMD, PIC::CHIP_COMMAND_PORTS[PIC::SLAVE]);
    return (ports::in(PIC::CHIP_COMMAND_PORTS[PIC::SLAVE]) << 8) | 
            ports::in(PIC::CHIP_COMMAND_PORTS[PIC::MASTER]);
}

/*
@ [?] clear ignorance status of an irq
@ [>] u8 irq: the irq whose status int he mask is to be cleared
@ [<] noreturn
*/
void PIC::clear_ignore_irq(u8 irq)
{
    KERNEL_LOG("IRQ: %d", irq);
    u8 mask = 0;
    bool chip = irq < 0x8 ? PIC::MASTER : PIC::SLAVE;
    irq &= 0x7;
    mask = get_mask(chip);
    mask = mask & (~(1 << irq));
    PIC::set_mask(chip, mask); 
}

/*
@ [?] remap the pic
@ [>] const u8& master_offset: the offset in the idt table for the master
@ [>] const u8& slave_offset: the offset in the idt table for the slave
@ [>] const u8& cascade_irq: the irq for cascading (usually 2)
@ [<] noreturn
*/
void PIC::remap(u8 master_offset, u8 slave_offset, u8 cascade_irq)
{
    KERNEL_LOG("master offset: %d, slave offset: %d, IRQ: %d", master_offset, slave_offset, cascade_irq);
    u8 master_mask = 0;
    u8 slave_mask = 0;
    master_mask = PIC::get_mask(PIC::MASTER);
    slave_mask = PIC::get_mask(PIC::SLAVE);
    ports::out(PIC::ICW1, CHIP_COMMAND_PORTS[PIC::MASTER]);
    ports::wait();
    ports::out(PIC::ICW1, CHIP_COMMAND_PORTS[PIC::SLAVE]);
    ports::wait();
    ports::out(master_offset, PIC::CHIP_DATA_PORTS[PIC::MASTER]);
    ports::wait();
    ports::out(slave_offset, PIC::CHIP_DATA_PORTS[PIC::SLAVE]);
    ports::wait();
    ports::out(1 << cascade_irq, PIC::CHIP_DATA_PORTS[PIC::MASTER]);
    ports::wait();
    ports::out(cascade_irq, PIC::CHIP_DATA_PORTS[PIC::SLAVE]);
    ports::wait();
    ports::out(PIC::ICW4, PIC::CHIP_DATA_PORTS[PIC::MASTER]);
    ports::wait();
    ports::out(PIC::ICW4, PIC::CHIP_DATA_PORTS[PIC::SLAVE]);
    ports::wait();
    PIC::set_mask(PIC::MASTER, master_mask);
    PIC::set_mask(PIC::SLAVE, slave_mask);
    
}
