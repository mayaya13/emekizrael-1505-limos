.intel_syntax noprefix

.equ param_ptr, 8

.section .text
    .globl idt_flush
    .type idt_flush, @function
idt_flush:
    push ebp
    mov ebp, esp
    mov eax, [ebp + param_ptr]
    lidt [eax]
    pop ebp
    ret
