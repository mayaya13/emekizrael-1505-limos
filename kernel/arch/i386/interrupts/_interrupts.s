.intel_syntax noprefix

.include "arch/i386/common/_asm_defs.s"

.extern isr_handler
.extern irq_handler

.macro ISR_NOERR isr_no
  .globl isr\isr_no
  .type isr\isr_no, @function
  isr\isr_no:
    cli
    push 0x0 # fake error code
    push \isr_no
    jmp isr_common
.endm

.macro ISR_ERR isr_no
  .globl isr\isr_no
  .type isr\isr_no, @function
  isr\isr_no:
    cli
    push \isr_no
    jmp isr_common
.endm

.macro IRQ irq_no, isr_no
  .globl irq\irq_no
  .type irq\irq_no, @function
  irq\irq_no:
    cli
    push 0x0 # fake error code
    push \isr_no
    jmp irq_common
.endm

.globl isr_common
.type isr_common, @function
isr_common:
  pushad
  mov ax, ds
  push eax
  mov ax, KERNEL_DATA_SELECTOR

  # FIXME: Not quite sure, should we do the stack segment aswell?
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	call isr_handler

	pop eax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	popad
	add esp, 8
	sti
	iret

.globl irq_common
.type irq_common, @function
irq_common:
	pushad

	mov ax, ds
	push eax

	mov ax, KERNEL_DATA_SELECTOR
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	
	call irq_handler

	pop ebx
	mov ds, bx
	mov es, bx
	mov fs, bx
	mov gs, bx

	popad
	add esp, 8
	sti
	iret

ISR_NOERR 0
ISR_NOERR 1
ISR_NOERR 2
ISR_NOERR 3
ISR_NOERR 4
ISR_NOERR 5
ISR_NOERR 6
ISR_NOERR 7
ISR_ERR   8
ISR_NOERR 9
ISR_ERR   10
ISR_ERR   11
ISR_ERR   12
ISR_ERR   13
ISR_ERR   14
ISR_NOERR 15
ISR_NOERR 16
ISR_ERR   17
ISR_NOERR 18
ISR_NOERR 19
ISR_NOERR 20
ISR_NOERR 21
ISR_NOERR 22
ISR_NOERR 23
ISR_NOERR 24
ISR_NOERR 25
ISR_NOERR 26
ISR_NOERR 27
ISR_NOERR 28
ISR_NOERR 29
ISR_ERR   30
ISR_NOERR 31
IRQ       0,  32
IRQ       1,  33
IRQ       2,  34
IRQ       3,  35
IRQ       4,  36
IRQ       5,  37
IRQ       6,  38
IRQ       7,  39
IRQ       8,  40
IRQ       9,  41
IRQ       10, 42
IRQ       11, 43
IRQ       12, 44
IRQ       13, 45
IRQ       14, 46
IRQ       15, 47

