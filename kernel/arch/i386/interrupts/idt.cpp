#include <kernel/logs.h>
#include <arch/i386/interrupts/idt.h>


IDT::idt_entry IDT::idt_entries[IDT::MAX_ENTRIES];

void IDT::insert(u32 index, u32 offset, u16 selector, u8 type)
{
    KERNEL_LOG("%d %x %x %x", index, offset, selector, type);
    IDT::idt_entry entry = IDT::idt_entry{.raw=0};
    entry.offset_low = offset & 0xFFFF;
    entry.selector = selector;
    entry.raw_type = type;
    entry.offset_high = offset >> 16;
    IDT::insert(index, entry);
}

/*
@ [?] Insert an entry to the idt
@ [>] const u32& index: the index for the entry to be inserted
@ [>] const idt_entry& entry: the idt_entry to be inserted
@ [<] noreturn
*/
void IDT::insert(u32 index, const idt_entry& entry)
{
    KERNEL_LOG("index: %d entry: %b %b", index, (entry.raw >> 32LL), entry.raw & 0xFFFFFFFFLL );
    idt_entries[index] = entry;
}

/*
@ [?] Update the cpu with the current idt
@ [<] noreturn
*/
void IDT::flush()
{
    IDT::idt_ptr_inners info;
    info.limit = sizeof(IDT::idt_entry) * IDT::MAX_ENTRIES - 1;
    info.base = (u32)IDT::idt_entries;
    idt_flush(&info);
}
