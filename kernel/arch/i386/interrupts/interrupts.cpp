#include <arch/i386/interrupts/interrupts.h>

const u8 Interrupts::CASCADE_IRQ = 2;
const u8 Interrupts::MASTER_OFFSET = 0x20;
const u8 Interrupts::SLAVE_OFFSET = 0x28;

Interrupts::interrupt_handler Interrupts::interrupt_handlers[IDT::MAX_ENTRIES];

ASM_FUNC void isr0(); ASM_FUNC void isr1(); 
ASM_FUNC void isr2(); ASM_FUNC void isr3(); ASM_FUNC void isr4();
ASM_FUNC void isr5(); ASM_FUNC void isr6(); ASM_FUNC void isr7();
ASM_FUNC void isr8(); ASM_FUNC void isr9(); ASM_FUNC void isr10(); 
ASM_FUNC void isr11(); ASM_FUNC void isr12(); ASM_FUNC void isr13();
ASM_FUNC void isr14(); ASM_FUNC void isr15(); ASM_FUNC void isr16();
ASM_FUNC void isr17(); ASM_FUNC void isr18(); ASM_FUNC void isr19();
ASM_FUNC void isr20(); ASM_FUNC void isr21(); ASM_FUNC void isr22(); 
ASM_FUNC void isr23(); ASM_FUNC void isr24(); ASM_FUNC void isr25();
ASM_FUNC void isr26(); ASM_FUNC void isr27(); ASM_FUNC void isr28(); 
ASM_FUNC void isr29(); ASM_FUNC void isr30(); ASM_FUNC void isr31();
ASM_FUNC void irq0(); ASM_FUNC void irq1(); ASM_FUNC void irq2(); 
ASM_FUNC void irq3(); ASM_FUNC void irq4(); ASM_FUNC void irq5();
ASM_FUNC void irq6(); ASM_FUNC void irq7(); ASM_FUNC void irq8(); 
ASM_FUNC void irq9(); ASM_FUNC void irq10(); ASM_FUNC void irq11();
ASM_FUNC void irq12(); ASM_FUNC void irq13(); ASM_FUNC void irq14();  
ASM_FUNC void irq15(); ASM_FUNC void isr0(); ASM_FUNC void isr1();


void Interrupts::initialize()
{
    for(u16 i = 0; i < IDT::MAX_ENTRIES; ++i)
    {
        Interrupts::interrupt_handlers[i] = nullptr;
    }
    Interrupts::setup_pic();
    Interrupts::init_idt();
}

static inline void main_interrupt_handler(const Interrupts::interrupt_context& context)
{
   //  KERNEL_LOG("%d", context.int_no);
    if(Interrupts::interrupt_handlers[context.int_no] == nullptr)
    {
        // FIXME: this is a completely retarded way of dealing with unhandled interrupts
        KERNEL_PANIC("UNHANDLED INTERRUPT! (int_no=%d)", context.int_no);
    }
    else
    {
        Interrupts::interrupt_handlers[context.int_no](context);
    }
}
NO_MANGLE void Interrupts::isr_handler(Interrupts::interrupt_context context)
{
    main_interrupt_handler(context);
}
NO_MANGLE void Interrupts::irq_handler(Interrupts::interrupt_context  context)
{
    main_interrupt_handler(context);
    PIC::end_of_interrupt(context.int_no >= Interrupts::SLAVE_OFFSET);
}
void Interrupts::register_handler(u8 int_no, const Interrupts::interrupt_handler& handler)
{
    Interrupts::interrupt_handlers[int_no] = handler;
}
void Interrupts::unregister_handler(u8 int_no)
{
    Interrupts::interrupt_handlers[int_no] = nullptr;
}
inline void Interrupts::setup_pic()
{
    KERNEL_LOG("remaping pic");
    PIC::remap(Interrupts::MASTER_OFFSET, Interrupts::SLAVE_OFFSET, Interrupts::CASCADE_IRQ);
    KERNEL_LOG("pic remapped!");
}

inline void Interrupts::init_idt()
{
    const u8 IRQ_INTERRUPT_FLAGS = ((u8) IDT::Descriptor_Type_Flags::PRESENT) | IDT::Descriptor_Type_Flags::FOUR_BYTE_INTERRUPT;
    const u8 ISR_INTERRUPT_FLAGS = ((u8) IDT::Descriptor_Type_Flags::PRESENT) | IDT::Descriptor_Type_Flags::FOUR_BYTE_TRAP;
    KERNEL_LOG("Entering exception isrs");
    IDT::insert(0, reinterpret_cast<u32>(isr0),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(1, reinterpret_cast<u32>(isr1),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(2, reinterpret_cast<u32>(isr2),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(3, reinterpret_cast<u32>(isr3),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(4, reinterpret_cast<u32>(isr4),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(5, reinterpret_cast<u32>(isr5),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(6, reinterpret_cast<u32>(isr6),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(7, reinterpret_cast<u32>(isr7),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(8, reinterpret_cast<u32>(isr8),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(9, reinterpret_cast<u32>(isr9),   GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(10, reinterpret_cast<u32>(isr10), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(11, reinterpret_cast<u32>(isr11), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(12, reinterpret_cast<u32>(isr12), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(13, reinterpret_cast<u32>(isr13), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(14, reinterpret_cast<u32>(isr14), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(15, reinterpret_cast<u32>(isr15), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(16, reinterpret_cast<u32>(isr16), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(17, reinterpret_cast<u32>(isr17), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(18, reinterpret_cast<u32>(isr18), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(19, reinterpret_cast<u32>(isr19), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(20, reinterpret_cast<u32>(isr20), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(21, reinterpret_cast<u32>(isr21), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(22, reinterpret_cast<u32>(isr22), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(23, reinterpret_cast<u32>(isr23), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(24, reinterpret_cast<u32>(isr24), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(25, reinterpret_cast<u32>(isr25), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(26, reinterpret_cast<u32>(isr26), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(27, reinterpret_cast<u32>(isr27), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(28, reinterpret_cast<u32>(isr28), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(29, reinterpret_cast<u32>(isr29), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(30, reinterpret_cast<u32>(isr30), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    IDT::insert(31, reinterpret_cast<u32>(isr31), GDT::CODE_SELECTOR, ISR_INTERRUPT_FLAGS);
    KERNEL_LOG("Entering irqs");
    IDT::insert(Interrupts::MASTER_OFFSET + 0, reinterpret_cast<u32>(irq0),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::MASTER_OFFSET + 1, reinterpret_cast<u32>(irq1),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::MASTER_OFFSET + 2, reinterpret_cast<u32>(irq2),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::MASTER_OFFSET + 3, reinterpret_cast<u32>(irq3),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::MASTER_OFFSET + 4, reinterpret_cast<u32>(irq4),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::MASTER_OFFSET + 5, reinterpret_cast<u32>(irq5),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::MASTER_OFFSET + 6, reinterpret_cast<u32>(irq6),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::MASTER_OFFSET + 7, reinterpret_cast<u32>(irq7),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::SLAVE_OFFSET +  8, reinterpret_cast<u32>(irq8),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::SLAVE_OFFSET +  9, reinterpret_cast<u32>(irq9),   GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::SLAVE_OFFSET +  10, reinterpret_cast<u32>(irq10), GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::SLAVE_OFFSET +  11, reinterpret_cast<u32>(irq11), GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::SLAVE_OFFSET +  12, reinterpret_cast<u32>(irq12), GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::SLAVE_OFFSET +  13, reinterpret_cast<u32>(irq13), GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::SLAVE_OFFSET +  14, reinterpret_cast<u32>(irq14), GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    IDT::insert(Interrupts::SLAVE_OFFSET +  15, reinterpret_cast<u32>(irq15), GDT::CODE_SELECTOR, IRQ_INTERRUPT_FLAGS);
    KERNEL_LOG("idt entries were inserted");
    KERNEL_LOG("flushing idt");
    IDT::flush();
    KERNEL_LOG("idt flushed");
}

