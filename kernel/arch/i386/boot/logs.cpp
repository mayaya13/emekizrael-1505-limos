#include <kernel/logs.h>
#include <libc/ports.h>

i32 Logger::log_char(i32 ic)
{
    const u8 PORT_HACK = 0xe9; // some hack usable in bochs & qemu for debugging
    ports::out((u8)ic, PORT_HACK);
    return ic;
}
static void do_nothing(u8, u8){}
i32 Logger::log(const char* __restrict format, ...)
{
    va_list parameters;
    va_start(parameters, format);
    i32 retval = printfmt<Logger::log_char, do_nothing>(format, parameters);
    va_end(parameters);
    return retval;
}
