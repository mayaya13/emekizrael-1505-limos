#include <kernel/timer.h>
#include <kernel/interrupts.h>
#include <arch/i386/interrupts/interrupts.h>

#define NO_OPERATION (void)0

u64 Timer::counter_channel0;

/*
@ [?] initializing the timer to work with the PIT.
@ [>] none
@ [<] none
*/
void Timer::initialize()
{
    KERNEL_LOG("Initializing Timer...");
    start_timer();
    PIT::init_pit(TIMER_FREQ, CLK_INT_PORT);
    KERNEL_LOG("Timer is set!");
}

void Timer::start_timer()
{
    Timer::counter_channel0 = 0;
    Interrupts::register_handler(CLK_INT_PORT, Timer::inc_counter0);
}

/*
@ [?] Increases TIMER::counter_channel0 everytime that IRQ0 occures. 
@ [>] an interrupt context.
@ [<] none
*/
void Timer::inc_counter0(Interrupts::interrupt_context)
{
    Timer::counter_channel0++;
}


/*
@ [?] A delay function 
@ [>] time of delay, each time unit = 10ms
@ [<] none
*/
void Timer::sleep(u64 time)
{
    u64 wait_time = Timer::counter_channel0;
    while((Timer::counter_channel0 - wait_time) < time)
    {
        asm volatile("hlt");
    }
}
