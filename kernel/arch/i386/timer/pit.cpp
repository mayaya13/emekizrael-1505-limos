#include <libc/ports.h>
#include <arch/i386/timer/pit.h>
#include <arch/i386/interrupts/interrupts.h>

#define PIT_FREQ 1193180

/*
aa comment something plz - this func initiates pit to fire IRQ0 every 10ms.
*/
void PIT::channel0_init(const u32 pit_freq)//1193180 hz
{
    u32 divisor = pit_freq / CLK_FREQUENCY;

    ports::out(PIT_INIT_CMD, PIT_CMD_REG); 
    ports::out((u8)(divisor & 0xFF), PIT_CHANNEL0);
    ports::out((u8)(divisor >> 8), PIT_CHANNEL0);
}


/*

*/
void PIT::init_pit(const u32 pit_freq, u32 irq_port)
{
    KERNEL_LOG("[PIT] Clock Frequency: [%d]", PIT_FREQ);
    channel0_init(pit_freq);
} 

/* We chose to ignore channels 1 & 2, but they exist. */