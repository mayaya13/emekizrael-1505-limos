# asm to enable paging and load the first page directory to CR3
.intel_syntax noprefix

.section .text
.global load_pd
.type load_pd, @function
load_pd:
    push ebp
    mov ebp, esp

    mov eax, [esp + 8]
    mov CR3, eax
    
    mov esp, ebp
    pop ebp
    ret
    
.section .text
.global enable_paging
enable_paging:

    push ebp
    mov ebp, esp

    mov eax, CR0
    or eax, 0x80000001
    mov CR0, eax

    mov esp, ebp
    pop ebp
    ret
