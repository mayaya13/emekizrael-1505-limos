#include <arch/i386/simplemm.h>
#include <kernel/interrupts.h>
#include <arch/i386/interrupts/interrupts.h>

#define PAGING_PORT 14
#define MEM_SIZE_16MB 0x1000000
#define PAGING_ENABLE_CMD 0x80000000

i32* SMM::frames;
i32 SMM::nframes;
i32 SMM::base_addr;
Paging::page_directory* SMM::kernel_dir;
Paging::page_directory* SMM::curr_dir;

void Paging::initialize()
{
    SMM::nframes = MEM_SIZE_16MB / SIZE_64_BS;
    SMM::frames = (i32*)SMM::kmalloc(BIT_INDEX(SMM::nframes), 1, 0);   
    memset(SMM::frames, 0, BIT_INDEX(SMM::nframes));

    SMM::kernel_dir = (page_directory*)SMM::kmalloc(sizeof(page_directory), 1,0);
    memset(SMM::kernel_dir, 0, sizeof(page_directory));
    SMM::curr_dir = SMM::kernel_dir;
   
    int i = 0;
    while (i < SMM::base_addr)
    {
        SMM::f_alloc(get_page(i, 1, SMM::kernel_dir), 0, 0); 
        i += SIZE_64_BS; 
    } 

    Interrupts::register_handler(PAGING_PORT, Paging::pfault_handler);
    load_pd((u32*)SMM::kernel_dir->physical_addr);
   //  enable_paging(); - problematic
    KERNEL_LOG("Paging is initialized!");
} 

/*
returns the wanted page or allocates a new one if make is true
*/
Paging::page_entry* Paging::get_page(i32 addr, int make, page_directory* dir)
{
    addr /= SIZE_64_BS;
    const i32 table_index = addr / SIZE_32_BS; 
    if(dir->tables[table_index]) // if its assigned 
        return &dir->tables[table_index]->pagies[addr%SIZE_32_BS];
    else if(make) 
    {
        // lets make a new one
        i32 tmp;
        dir->tables[table_index] = (page_table*)SMM::kmalloc(sizeof(page_table), 1, &tmp);
        memset(dir->tables[table_index], 0, SIZE_64_BS);

        dir->physical_tables_addrs[table_index] = tmp | 0x07; // setting the page frame - present, rw, us
        return &dir->tables[table_index]->pagies[addr%SIZE_32_BS];
    }
    else
        return 0;
}

/*
handles the page fault
*/
void Paging::pfault_handler(Interrupts::interrupt_context i_context)
{
    u32 bad_addr;
    asm("mov %%cr2, %0" : "=r"(bad_addr));
    KERNEL_LOG("A page fault occurred...");
    KERNEL_PANIC("PAGE FAULT at 0x%x", bad_addr);
}