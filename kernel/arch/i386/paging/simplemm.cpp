#include <arch/i386/simplemm.h>

/*
allocates memory 
*/
i32 SMM::kmalloc(u32 size, int aligned, i32* physical_addr)
{
    if(aligned == 1 && (SMM::base_addr & 0x00000FFF))
    {
        // algining the address
        SMM::base_addr &= 0x00000FFF;
        SMM::base_addr += SIZE_64_BS; 
    }
    if(physical_addr)
        *physical_addr = SMM::base_addr;
    i32 tmp_addr = SMM::base_addr;
    SMM::base_addr += size;
    return tmp_addr;    
}

/*
finds the first free frame in our bitset
*/
i32 SMM::f_first()
{
    i32 i, j;
    for(i = 0; i < BIT_INDEX(SMM::nframes); i++)
    {
        if(SMM::frames[i] != 0xFFFF)
        {
            for(j = 0; j < 32; j++)
                if(!(SMM::frames[i] & (0x1 << j)))
                    return i*SIZE_4_BYTES + j;
        }
    }
}

/*
sets a bit in the frames bit set to say that we have a new frame
*/
void SMM::f_set(i32 frame_addr)
{
    i32 framie = frame_addr / SIZE_64_BS;
    i32 indiex = BIT_INDEX(framie);
    i32 offset = BIT_OFFSET(framie);
    SMM::frames[indiex] |= (0x1 << offset);  
}

/*
allocates a new pafe frame 
*/
void SMM::f_alloc(Paging::page_entry* page, int ku, int rw)
{
    if(page->frame != 0)
        return; // this frame exists already
    else
    {
        // allocate a new framee
        i32 frame_idx = SMM::f_first();
        KERNEL_LOG("frame_idx: %d, %d", frame_idx, (i32) (-1));
        KERNEL_ASSERT_CMD((!(frame_idx == (i32)(-1))), "we got no space left in memory loolie");
        SMM::f_set(frame_idx*SIZE_64_BS);
        // now lets set the frame
        page->frame = frame_idx;
        page->present = 1;
        page->rw = (u32)rw;
        page->user = (u32)ku;
    }
}