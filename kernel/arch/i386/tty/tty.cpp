#include <libc/common.h>
#include <libc/string.h>
 
#include <arch/i386/tty/tty.h>
#include <kernel/logs.h>

static const u64 VGA_WIDTH = 80;
static const u64 VGA_HEIGHT = 25;
static u16* const VGA_MEMORY = (u16*) 0xB8000;
 
static u64 terminal_row;
static u64 terminal_column;
static u8 terminal_color;
static u16* terminal_buffer;
 
void terminal_setcolor(u8 color)
{
	terminal_color = color;
}

void TTY::setcolor(u8 fg, u8 bg)
{
	KERNEL_LOG("fg: %u bg: %u", fg, bg);
	terminal_setcolor(TTY::vga_entry_color(Vga_Color(fg), Vga_Color(bg)));
}
void terminal_putentryat(u8 c, u8 color, u32 x, u32 y)
{
	terminal_buffer[y * VGA_WIDTH + x] = TTY::vga_entry(c, color);
}

void TTY::clear()
{
    const u8 INITIAL_CHAR = ' ';
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = TTY::vga_entry_color(TTY::Vga_Color::GREEN, TTY::Vga_Color::BLACK);
	terminal_buffer = VGA_MEMORY;
    for (u64 y = 0; y < VGA_HEIGHT; y++)
	{
        for (u64 x = 0; x < VGA_WIDTH; x++)
		{
            const u64 index = y * VGA_WIDTH + x;
			terminal_buffer[index] = vga_entry(INITIAL_CHAR, terminal_color);
		}
	}
}
void TTY::initialize()
{
	TTY::clear();
}

i32 TTY::putchar(i32 uc)
{
	const i32 success = 0;
	u8 character = (u8) uc;
	if(character == '\n')
	{
		terminal_column = 0;
		if (++terminal_row == VGA_HEIGHT)
		{
			terminal_row = 0;
		}
	}
	else
	{
		terminal_putentryat(character, terminal_color, terminal_column, terminal_row);
		if (++terminal_column == VGA_WIDTH)
		{
			terminal_column = 0;
			if (++terminal_row == VGA_HEIGHT)
			{

				terminal_row = 0;
			}
		}
	}
	return success;
}

void TTY::write(const u8* data, u64 size)
{
    for (u32 i = 0; i < size; i++)
	{
		TTY::putchar(data[i]);
	}
}

void TTY::writestring(const u8* data)
{
	TTY::write(data, strlen((const char*)data));
}
