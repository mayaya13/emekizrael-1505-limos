#pragma once

#include <kernel/tty.h>

namespace TTY
{
    enum class Vga_Color : u8
    {
        BLACK = 0,
        BLUE = 1,
        GREEN = 2,
        CYAN = 3,
        RED = 4,
        MAGENTA = 5,
        BROWN = 6,
        LIGHT_GREY = 7,
        DARK_GREY = 8,
        LIGHT_BLUE = 9,
        LIGHT_GREEN = 10,
        LIGHT_CYAN = 11,
        LIGHT_RED = 12,
        LIGHT_MAGENTA = 13,
        LIGHT_BROWN = 14,
        WHITE = 15,
    };
    
    inline u8 vga_entry_color(const Vga_Color& fg, const Vga_Color& bg)
    {
        return ((u8)fg) | ((u8)bg) << 4;

    }
    inline u16 vga_entry(u8 character, u8 color)
    {
        return ((u16) character) | (((u16) color) << 8);
    }
}
