#pragma once

#include <libc/ports.h>
#include <kernel/osdefs.h>
#include <arch/i386/interrupts/interrupts.h>

#define PIT_CHANNEL0 0x40
#define PIT_CHANNEL2 0x42
#define PIT_CMD_REG 0x43

#define CLK_FREQUENCY 10000 // 100hz - 10ms
#define PIT_INIT_CMD 0x36 // initial command byte for PITush to recieve data


namespace PIT
{
    void channel0_init(const u32 pit_freq);
    void init_pit(const u32 pit_freq, u32 irq_port);
}