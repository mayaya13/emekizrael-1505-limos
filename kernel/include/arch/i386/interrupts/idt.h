#pragma once

#include <libc/common.h>
#include <kernel/osdefs.h>
#include <kernel/logs.h>

namespace IDT
{

    enum Descriptor_Type_Flags : u8
    {
        FOUR_BYTE_TASK = 0x5,
        TWO_BYTE_INTERRUPT = 0x6,
        TWO_BYTE_TRAP = 0x7,
        FOUR_BYTE_INTERRUPT = 0xE,
        FOUR_BYTE_TRAP = 0xF,
        STORAGE_SEGMENT = 1 << 4,
        TASK = 1 << 4,
        INTERRUPT = 0,
        TRAP = 0,
        USERMODE = (1 << 5) | (1 << 6),
        PRESENT = 1 << 7
    };
    union idt_entry
    {
        uint64_t raw;
        struct
        {
            u16 offset_low;
            u16 selector;
            u8 zero;
            union 
            {
                u8 raw_type;
                struct 
                {
                    u8 gate_type : 4;
                    u8 storage_segment : 1;
                    u8 descriptor_privilege_level : 2;
                    u8 is_present : 1;
                } NO_ALIGNMENT;
                
            } NO_ALIGNMENT;
            u16 offset_high;
        } NO_ALIGNMENT;
    } NO_ALIGNMENT;

    const u16 MAX_ENTRIES = 256;
    extern idt_entry idt_entries[IDT::MAX_ENTRIES];
    void insert(u32 index, u32 offset, u16 selector, u8 type);
    void insert(u32 index, const idt_entry& entry);
    void flush();

    struct idt_ptr_inners
    {
        u16 limit;
        u32 base;
    } NO_ALIGNMENT;
};
ASM_FUNC void  idt_flush(IDT::idt_ptr_inners* ptr);
