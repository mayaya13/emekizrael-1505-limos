#pragma once

#include <arch/i386/architecture/gdt.h>
#include <kernel/interrupts.h>
#include <arch/i386/interrupts/pic.h>
#include <arch/i386/interrupts/idt.h>


namespace Interrupts
{
    extern const u8 CASCADE_IRQ;
    extern const u8 MASTER_OFFSET;
    extern const u8 SLAVE_OFFSET;
    inline void setup_pic();
    inline void init_idt();

    struct interrupt_context
    {
        u32 ds;
        u32 edi, esi, ebp, esp, ebx, edx, ecx, eax;
        u32 int_no; u32 err_code; 
        u32 eip, cs, eflags, useresp, ss;
    } NO_ALIGNMENT;

    using interrupt_handler = void (*)(interrupt_context);
    extern interrupt_handler interrupt_handlers[IDT::MAX_ENTRIES];
    NO_MANGLE void isr_handler(interrupt_context context);
    NO_MANGLE void irq_handler(interrupt_context context);
    void register_handler(u8 int_no, const interrupt_handler& handler);
    void unregister_handler(u8 int_no);
}
