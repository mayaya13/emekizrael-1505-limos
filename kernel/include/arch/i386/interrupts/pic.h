#pragma once

#include <libc/ports.h>
#include <libc/common.h>
#include <kernel/osdefs.h>
#include <kernel/logs.h>

namespace PIC
{
    extern const u16 CHIP_COMMAND_PORTS[2];
    extern const u16 CHIP_DATA_PORTS[2];
    extern const u8 END_OF_INTERRUPT_CMD;
    extern const u8 ICW1;
    extern const u8 ICW4;
    extern const u8 CHIP_IDT_OFFSETS[2];
    extern const u8 READ_IRR_CMD;
    extern const u8 READ_ISR_CMD;
    extern const bool MASTER;
    extern const bool SLAVE;
    u8 get_mask(bool is_slave);
    void set_mask(bool is_slave, u8 mask);
    void ignore_irq(u8 irq);
    void clear_ignore_irq(u8 irq);
    u16 ger_irr();
    u16 get_isr();
    void end_of_interrupt(bool is_slave_interrupt);
    void remap(u8 master_offset, u8 slave_offset, u8 cascade_irq); // https://www.eeeguide.com/8259-programmable-interrupt-controller/
}
