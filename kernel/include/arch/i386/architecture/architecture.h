#pragma once

#include <kernel/architecture.h>
#include <arch/i386/architecture/gdt.h>

namespace Architecture 
{
    extern const u32 NULL_SELECTOR;
    extern const u32 CODE_SELECTOR;
    extern const u32 DATA_SELECTOR;
    extern const u32 CODE_FLAGS;
    extern const u32 DATA_FLAGS;
    extern const u32 CODE_LIMIT;
    extern const u32 DATA_LIMIT;
    extern const u32 CODE_BASE;
    extern const u32 DATA_BASE;
    void init_gdt();
}
