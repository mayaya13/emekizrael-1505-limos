#pragma once

#include <libc/common.h>
#include <kernel/osdefs.h>
#include <kernel/logs.h>

namespace GDT 
{
    extern const u32 MAX_ADDRESSABLE_UNIT;
    extern const u8 USERMODE_DPL;
    extern const u8 KERNELMODE_DPL;
    extern const u32 BASE_ADDRESS;

    // flat mode
    extern const u32 CODE_SELECTOR;
    extern const u32 DATA_SELECTOR;
    extern const u32 CODE_FLAGS;
    extern const u32 DATA_FLAGS;
    extern const u32 CODE_LIMIT;
    extern const u32 DATA_LIMIT;
    extern const u32 CODE_BASE;
    extern const u32 DATA_BASE;

    union gdt_entry
    {
        uint64_t raw;
        struct
        {
            u16 limit_low;
            u16 base_low;
            u8 base_middle;

            u8 is_accessed : 1; // THIS BIT IS SET BY THE CPU WHEN THE SEGMENT IS BEING ACCESSES. SET TO 0
            u8 rw_bit : 1;
            u8 dc_bit : 1;
            u8 executable_bit : 1;
            u8 descriptor_type : 1;
            u8 descriptor_privilege_level : 2;
            u8 is_present : 1;

            u8 limit_high : 4;

            u8 extra_flag_bit : 1;
            u8 long_bit : 1;
            u8 size_bit : 1;
            u8 granularity : 1;

            u8 base_high;
        } NO_ALIGNMENT;
    } NO_ALIGNMENT;

    extern const gdt_entry NULL_DESCRIPTOR_ENTRY;
    extern const u32 NULL_SELECTOR;
    enum GDT_Flags : u32
    {
        READ = 1 << 1,
        WRITE = 1 << 1,
        DIRECTION = 1 << 2,
        CONFORMING = 1 << 2,
        EXECUTABLE = 1 << 3,
        USERMODE = 1 << 4,
        KERNELMODE = 0,
        CODE_SEGMENT = 1 << 5,
        DATA_SEGMENT = 1 << 5,
        PRESENT = 1 << 6,
        SYSTEM_SEGMENT = 0,
        FOUR_BYTE_SEGMENT = 1 << 7,
        TWO_BYTE_SEGMENT = 0,
        BYTE_GRANULARITY = 0,
        PAGE_GRANULARITY = 1 << 8
    };
    const u16 MAX_ENTRIES = 3;
    extern gdt_entry gdt_entries[GDT::MAX_ENTRIES];

    void insert(u32 index, const gdt_entry& entry);
    void insert(u32 index, u32 limit, u32 base, u32 flags);
    void flush();
    void setup();

    struct gdt_ptr_inners
    {
        u16 limit;
        u32 base;
    } NO_ALIGNMENT;
}

ASM_FUNC void gdt_flush(GDT::gdt_ptr_inners* ptr);
