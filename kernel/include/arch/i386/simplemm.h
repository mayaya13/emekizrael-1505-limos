/* Simple Memory Management for Paging*/
#include <kernel/osdefs.h>
#include <arch/i386/paging.h>

#define FOUR_BYTES 32
#define SIZE_4_BYTES 8*4
#define BIT_INDEX(a) (a/FOUR_BYTES)
#define BIT_OFFSET(a) (a%FOUR_BYTES)

/*
 A simple memory management unit for page frames
 yay
*/
namespace SMM
{
    extern i32* frames;
    extern i32 nframes;
    extern i32 base_addr;
    extern Paging::page_directory* kernel_dir;
    extern Paging::page_directory* curr_dir;

    i32 kmalloc(u32 size, int aligned, i32* physical_addr);

    static void f_set(i32 frame_addr);
    static void f_clear(i32 frame_addr);
    static void f_test(i32 frame_addr);
    i32 f_first();

    void f_alloc(Paging::page_entry* page, int ku, int rw);
    void f_dealloc(Paging::page_entry* framie);
}
