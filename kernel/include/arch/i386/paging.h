// #include <libc/stddef.h>
#include <libc/common.h>
#include <kernel/interrupts.h>
#include <arch/i386/interrupts/interrupts.h>

#define SIZE_32_BS 1024
#define SIZE_64_BS 0x1000


namespace Paging
{
   typedef struct page_entry
   {
      u32 present    : 1;   // Is the page present in memory
      u32 rw         : 1;   // Read & write if set, read-only otherwise 
      u32 user       : 1;   // User mode if set, Kernel mode ottherwise
      u32 accessed   : 1;   // Has the page been accessed since last refresh?
      u32 dirty      : 1;   // Has the page been written to since last refresh?
      u32 unused     : 7;   // Available 3 bits for kernel use
      u32 frame      : 20;  // Physical address of frame 
   }page_entry;

   typedef struct page_table
   {
      page_entry pagies[SIZE_32_BS];
   }page_table;

   typedef struct page_directory
   {
      page_table* tables[SIZE_32_BS]; // pointers to page tables
      i32 physical_tables_addrs[SIZE_32_BS]; // pointers to physical addresses of pagetables 
      i32 physical_addr; // phsical address of the array above
   }page_directory;

   void initialize();
   page_entry* get_page(i32 addr, int make, page_directory* dirie);
   void pfault_handler(Interrupts::interrupt_context i_context);

}

ASM_FUNC void enable_paging();
ASM_FUNC void load_pd(u32* pdidi);
