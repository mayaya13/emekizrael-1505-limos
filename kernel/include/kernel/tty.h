#pragma once

#include <libc/common.h>

namespace TTY
{
    void clear();
    void initialize();
    i32 putchar(i32 uc);
    void setcolor(u8 fg, u8 bg);
    void write(const u8* data, u64 size);
    void writestring(const u8* data);
}
