#pragma once

#include <kernel/osdefs.h>

namespace Architecture
{
    extern const char* NAME;
    extern const u32 PAGE_FRAME_BYTES;
    void initialize();
}
