#pragma once

#include <kernel/osdefs.h>
#include <libc/stdio.h>

namespace Logger
{
    // FIXME: after interrupts finally work use COM1 for debugging instead of the dumb port hack
    i32 log_char(i32);
    i32 log(const char* __restrict format, ...);
}

#ifdef __KERNEL_LOG
    #define KERNEL_LOG(fmt, ...)                                                                         \
        Logger::log("[" __FILE__ "::");                                                                  \
        Logger::log(__FUNCTION__);                                                                       \
        Logger::log(" line:" TOSTRING(__LINE__) "] ");                                                   \
        Logger::log(fmt VA_ARGS(__VA_ARGS__));                                                                   \
        Logger::log("\n");
#else
    #define KERNEL_LOG(fmt, ...)
#endif
