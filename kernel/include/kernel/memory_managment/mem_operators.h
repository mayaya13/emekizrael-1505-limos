#pragma once
#include <kernel/osdefs.h>
#include <stddef.h>

void *operator new(u32 size);
void *operator new[](u32 size);
void operator delete(void *p);
void operator delete[](void *p);
void operator delete(void *p,u32);
void operator delete[](void *p,u32);