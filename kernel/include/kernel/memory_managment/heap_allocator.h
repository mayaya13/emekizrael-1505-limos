#pragma once

#include <kernel/osdefs.h>

namespace MemoryManagment
{
    struct HeapListNode
    {
        u32 size;
        HeapListNode* next;
        HeapListNode(u32 size, HeapListNode* next) : size(size), next(next) {}
        void* start_addr()
        {
            return reinterpret_cast<void*>(this);
        }
        void* end_addr()
        {
            return (void*)(((u8*)this->start_addr()) + this->size - 1);
        }
    };
    class HeapAllocator  // FIXME: use buddy allocation like linux or some segment tree ds, linked list is slow and sucks, implemented this version because we need to present something by 21.2
    {
    public:
        HeapAllocator(void* heap_start, u32 heap_size);
        ~HeapAllocator();
        void initialize();
        void* allocate(u32 bytes);
        void* z_allocate(u32 bytes);
        void deallocate(void* addr);
        u32 get_heap_size();
        void* get_heap_start();
        // FIXME: Add reallocation. we need to consider implementation

    private:


        void add_free_region(void* addr, u32 size);
        HeapListNode* find_region(u32 size);
        void* const m_heap_start;
        u32 m_heap_size;
        HeapListNode* m_head;

    };
}
