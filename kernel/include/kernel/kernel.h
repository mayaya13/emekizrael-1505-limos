#pragma once

#include <libc/stdio.h>
#include <libc/common.h>

#include <kernel/tty.h>
#include <kernel/logs.h>
#include <kernel/timer.h>
#include <kernel/osdefs.h>
#include <kernel/interrupts.h>
#include <kernel/architecture.h>
#include <kernel/memory_managment/heap_allocator.h>
namespace Kernel
{
    NO_MANGLE void kernel_main(void);
    extern MemoryManagment::HeapAllocator* allocator;
}
