#pragma once

#include <libc/stdio.h>
#include <libc/common.h>
#include <kernel/memory_managment/mem_operators.h>

#define INF_LOOP()              \
    asm volatile ("cli");       \
    while(true)                 \
    {                           \
        asm volatile( "hlt");   \
    }                           \


#define KERNEL_PANIC(details, ...)               \
    printf("[CRITICAL] Kernel PANIC!\n");        \
    printf("::FILE: " __FILE__ "\n");            \
    printf("::LINE: " TOSTRING(__LINE__) "\n");  \
    printf(details VA_ARGS(__VA_ARGS__));                \
    printf("\n");                                 \
    INF_LOOP();                                  \

#define KERNEL_ASSERT(condition) if(!(condition)) {KERNEL_PANIC("[?] Assertion Failed :: " #condition "\n");}
#define KERNEL_ASSERT_CMD(condition, panic_text) if(!(condition)) {KERNEL_PANIC(" OH NOOOO! :: " #panic_text "\n");}
