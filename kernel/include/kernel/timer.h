#pragma once

#include <kernel/osdefs.h>
#include <arch/i386/timer/pit.h>
#include <kernel/logs.h>

#define CLK_INT_PORT 32
#define TIMER_FREQ 1193180

namespace Timer 
{
    extern u64 counter_channel0;
    void initialize();
    void start_timer();
    void sleep(u64 time);
   // u64 seconds_count();
   // u64 milliSeconds_count();
    void inc_counter0(Interrupts::interrupt_context);
}
