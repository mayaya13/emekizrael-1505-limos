#include <kernel/kernel.h>
#include <kernel/tty.h>
#include <kernel/logs.h>
#include <kernel/timer.h>
#include <kernel/osdefs.h>
#include <kernel/interrupts.h>
#include <kernel/architecture.h>
#include <arch/i386/paging.h>

void print_inits();

// showing that the heap works
static MemoryManagment::HeapAllocator __allocator((void*)0xD0000000, 100 * 4096); // strating size 100 pages, located at D0
MemoryManagment::HeapAllocator* Kernel::allocator = &__allocator;

NO_MANGLE void Kernel::kernel_main(void)
{
    asm volatile("cli");
    //
	TTY::initialize();
    Architecture::initialize();
    Interrupts::initialize();
    Timer::initialize();
    Paging::initialize();
    KERNEL_LOG("HEAP - ALLOCATOR GLOBAL DATA: %x %d", allocator->get_heap_start(), allocator->get_heap_size());
    Kernel::allocator->initialize();
    //
    asm volatile("sti");
    
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    printf("$C                        88     88 8b    d8  dP\"Yb  .dP\"Y8\n", 2, 0);
    printf("$C                        88     88 88b  d88 dP   Yb `Ybo.\" \n", 2, 0); 
    printf("$C                        88  .o 88 88YbdP88 Yb   dP o.`Y8b \n", 2, 0); 
    printf("$C                        88ood8 88 88 YY 88  YbodP  8bodP'  \n", 2, 0); 
    
    print_inits();
    
    /* KERNEL_LOG("Allocating memoryy");
    int* x = new int [5];
    printf("%x\n", x);
    int* y = new int [5];
    printf("%x\n", y); */
    INF_LOOP();
}

/*
Function prints things that make this OS look OK lol
*/
void print_inits()
{
    KERNEL_LOG("Printing all initializations to screen");
    printf("$C", 13, 0);
    printf("%s\n", Architecture::NAME);
    printf("lol we suck\n");
    printf("                                 WELCOME\n                        To Our Operating System :)\n\n\n");
    printf("{*} LimOS {*}\n\n");
    Timer::sleep(10000);
    printf("[*] Initializing GDT.");
    Timer::sleep(2000); printf(".");Timer::sleep(1000);printf(".");Timer::sleep(1000);printf(".");Timer::sleep(3000);
    printf("GDT initialized!\n");
    Timer::sleep(6000);

    printf("[*] Initializing PIC.");
    Timer::sleep(2000); printf(".");Timer::sleep(1000);printf(".");Timer::sleep(1000);printf(".");Timer::sleep(3000);
    printf("PIC initialized!\n");
    Timer::sleep(6000);

    printf("[*] Initializing IDT.");
    Timer::sleep(2000); printf(".");Timer::sleep(1000);printf(".");Timer::sleep(1000);printf(".");Timer::sleep(3000);
    printf("IDT initialized!\n");
    Timer::sleep(6000);

    printf("[*] Initializing Timer.");
    Timer::sleep(2000); printf(".");Timer::sleep(1000);printf(".");Timer::sleep(1000);printf(".");Timer::sleep(3000);
    printf("Timer initialized!\n");
    Timer::sleep(6000);

    printf("[*] Initializing Paging.");
    Timer::sleep(2000); printf(".");Timer::sleep(1000);printf(".");Timer::sleep(1000);printf(".");Timer::sleep(3000);
    printf("Paging initialized!\n");
    Timer::sleep(6000);
    
    printf("[*] Initializing Heap.");
    Timer::sleep(2000); printf(".");Timer::sleep(1000);printf(".");Timer::sleep(1000);printf(".");Timer::sleep(3000);
    printf("Heap initialized!\n");
    Timer::sleep(6000);

    printf("\n<*> Thanks <3 <*>\n");
}
