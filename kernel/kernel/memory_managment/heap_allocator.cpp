#include <kernel/memory_managment/heap_allocator.h>
#include <kernel/logs.h>
#include "kernel/architecture.h"
namespace MemoryManagment
{
HeapAllocator::HeapAllocator(void* heap_start, u32 heap_size) :
    m_heap_start(heap_start), m_heap_size(heap_size), m_head(nullptr)
{
}
HeapAllocator::~HeapAllocator(){};

void HeapAllocator::initialize()
{
    KERNEL_LOG("%x %d", this->m_heap_start, this->m_heap_size);
    // TODO: map pages for the heap address range using maya's page fram allocator
    this->add_free_region(this->m_heap_start, this->m_heap_size);
    KERNEL_LOG("Heap is initialized!");
}
void HeapAllocator::add_free_region(void* addr, u32 size)
{
    // FIXME: we should perhaps check for aligment issues and that the area is valid, this works for now
    *(reinterpret_cast<HeapListNode*>(addr)) = HeapListNode(size - sizeof(HeapListNode), (this->m_head==nullptr)?nullptr:this->m_head);
    this->m_head = reinterpret_cast<HeapListNode*>(addr);
}
HeapListNode* HeapAllocator::find_region(u32 size)
{
    HeapListNode* curr = this->m_head;
    HeapListNode* prev = nullptr;
    KERNEL_LOG("%x", curr);
    while(curr != nullptr)
    {
        KERNEL_LOG("%x %d", curr, curr->size);
        KERNEL_ASSERT(curr->next == nullptr);
        if(curr->size >= size) {
            if(prev != nullptr) {
                prev->next = curr->next;
            } else {
                this->m_head = nullptr;
            }
            return curr;
        }
        curr = curr->next;
        prev = (prev == nullptr) ? this->m_head : prev->next;
    }
    return nullptr;
}
void* HeapAllocator::get_heap_start()
{
    return this->m_heap_start;
}

u32 HeapAllocator::get_heap_size()
{
    return this->m_heap_size;
}

void* HeapAllocator::allocate(u32 bytes)
{
    auto region = this->find_region(bytes);
    if(region == nullptr)
    {
        return nullptr; // STUB!
        // TODO: use page frame allocator for big allocation, increase the heap size
    }
    if(region->size - bytes > sizeof(HeapListNode))
    {
        this->add_free_region((u8*)region->end_addr() + 1, region->size  - bytes);
    }
    return ((u8*)region->start_addr()) + sizeof(HeapListNode);
}
void* HeapAllocator::z_allocate(u32 bytes)
{
    return (void*)0xdeadbeef;  // FIXME: when we actually need z_allocate, use memset or something
}
void HeapAllocator::deallocate(void* addr)
{
    HeapListNode* free_block_ptr = reinterpret_cast<HeapListNode*>(((u8*)addr)-sizeof(HeapListNode));
    this->add_free_region(free_block_ptr, free_block_ptr->size);
}
}