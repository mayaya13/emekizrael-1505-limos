#include <kernel/memory_managment/mem_operators.h>
#include <kernel/kernel.h>
#include <kernel/logs.h>


void *operator new(u32 size)
{
    KERNEL_LOG("new called");
    return Kernel::allocator->allocate(size);
}
void *operator new[](u32 size)
{
    KERNEL_LOG("new[] called");
    return Kernel::allocator->allocate(size);
}
void operator delete(void *p)
{
    Kernel::allocator->deallocate(p);
}
void operator delete[](void *p)
{
    Kernel::allocator->deallocate(p);
}
void operator delete(void *p,u32){
    KERNEL_PANIC("why is this called??? weird");
}
void operator delete[](void *p,u32){
    KERNEL_PANIC("why is this called??? weird");
}