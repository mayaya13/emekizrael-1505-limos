Our Design Rules & Conventions:
-----------------------------------------------------------

Variables & Structs:

pic_master - Good
PIC_master - Eh
picMaster - Bad

Enums:

Always prefer enum class over enum, **but if the enum is for a flag bitmap, NEVER use enum class, otherwise you have to cast and it's very annoying**
Always inherit from the type of the elements of the enum
You_Should_Write_Enum_Names_Like_This

enum GDT_Flags : u8 - Good
enum class GDT_Flags : u8 - Bad, casting is annoying
enum GDT_Flags - Bad, element type not specified
enum gdtFLags : u8 - Bad

Class VS Namespace:

When developing an operating system, one needs alot of global items, such as Descriptor Tables, etc. These should not be bloated thereby stored inside a namespace.
If you intend to create something one can create multiple instances of and it doesn't have to be global, that implies the usage of a class instead

Classes & Enum Syntax:
WriteClassAndNamespaceNamesLikeThis
tty - Bad
Tty - Good
TTY - Best, since the term itself is a shortcut (Teletype..)

my_class - NEVER write a class name like this

